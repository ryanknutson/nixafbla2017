$(function() {
    $('.navbar-nav').children().addClass("hvr-underline-reveal");
    //$('.my-4').parent().show().addClass("animated fadeInUp");
    $('.navbar-brand').addClass("hvr-underline-from-center");
    //resizeFn();
    
    $(window).smartresize(function(){
        //resizeFn();
    });
});

function resizeFn() {
        var windowWidth = $(document).width();
        var navToggleMargin = ((25/28) * windowWidth)-(1510/7);
        $(".navbar-toggler").css("margin-left", navToggleMargin + "px");

            if ($(window).width() <= 768) {
                $(".leftoftwitter").css("margin-left", ($(".leftoftwitter").parent().width() - $(".leftoftwitter").width()) / 2);
            } else {
                $(".leftoftwitter").css("margin-left", "0");
                $(".box").find("p").matchHeight();
            }
}

(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 110);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
