var fs = require('fs');
var pug = require('pug');
var async = require('async');
var getCodeBlock = require('pug-code-block');
var concat = require('concat-files');
const replace = require('replace-in-file');

const options = {
  files: [
    './src/_onepage/_resume_cv.pug',
    './src/_onepage/_applications.pug',
    './src/_onepage/_interviews.pug'
  ],
  from: /block content/g,
  to: '\n',
};


function getCode(src) {
    var src2 = "./src/_onepage/_" + src.split('/')[2];
    console.log(src2);
    fs.writeFileSync(src2,
        getCodeBlock.byString(
            fs.readFileSync(src,'utf8'),
            'block content'
        )
    );
}

writeFiles();

function writeFiles() {
    getCode('./src/job_openings.pug');
    getCode('./src/resume_cv.pug');
    getCode('./src/applications.pug');
    getCode('./src/interviews.pug');
    replaceContent();
}


function concatfiles() {
    concat([
    './src/_onepage/_template.pug',
    './src/_onepage/_job_openings.pug',
    './src/_onepage/_resume_cv.pug',
    './src/_onepage/_applications.pug',
    './src/_onepage/_interviews.pug'
    ], './src/_onepage/_onepage.pug', function(err) {
        if (err) throw err
        console.log('done');
        renderPug();
    });
    
}




function replaceContent() {
    replace(options, (error, changes) => {
      if (error) {
        return console.error('Error occurred:', error);
      }
      console.log('Modified files:', changes.join(', '));
      concatfiles();
    });
}


function renderPug() {
    var html = pug.renderFile('./src/_onepage/_onepage.pug');
    fs.writeFile('./src/onepage.html', html);
}