var gulp = require("gulp");
var pug = require("gulp-pug");
var sass = require('gulp-sass');
var syncy = require('syncy');
var minify = require('gulp-minify');

gulp.task('pug', function buildHTML(){
    gulp.src(['src/*.pug'], {
        base: 'src'
    })
    .pipe(pug())
    .pipe(gulp.dest('./dist/'));
});


gulp.task('synconepage', function buildHTML(){
    gulp.src(['src/onepage.html'], {
        base: 'src'
    })
    .pipe(gulp.dest('./dist/'));
});


gulp.task('sass', function () {
  return gulp.src('src/css/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./dist/css/'));
});
 

gulp.task('syncimg', (done) => {
    syncy(['src/img/**/*.*'], 'dist/img', {base: 'src/img'})
    .then(() => {
      done();
    })
    .catch((err) => {
      done(err);
    });
});



gulp.task('syncjs', (done) => {
    syncy(['src/js/**/*.*'], 'dist/js', {base: 'src/js'})
    .then(() => {
      done();
    })
    .catch((err) => {
      done(err);
    });
});

gulp.task('watch', ['sass', 'pug', 'syncimg', 'syncjs', 'compress'], function() {
  console.log('Gulp watch is running!');
  gulp.watch('./sass/**/*.scss', ['sass']);
  gulp.watch('./src/**/*.pug', ['pug']);
});

gulp.task('default', ['sass', 'pug', 'syncimg', 'synconepage', 'compress']);

gulp.task('compress', function() {
  gulp.src('src/js/*.js')
    .pipe(minify({
        ext:{
            src:'.js',
            min:'-min.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('dist/js'))
});